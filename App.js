import * as React from "react";
import { View, Text } from "react-native";
import Login from './pages/login';
import Profile from './pages/profile';
import Home from './pages/home';
import Detail from './pages/detail';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { createStore } from 'redux'

// Membuat action types
const types = {
    INCREMENT: 'INCREMENT',
  }
  
  // Membuat reducer
  const reducer = (state, action) => {
    if (action.type === types.INCREMENT) {
      return { count: state.count + 1 }
    }
    return state
  }
  
  // Mendefinisikan initial state dari store
  const initialState = { count: 0 }

const Stack = createStackNavigator();
const AuthStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const HomeDetailScreen = () => (
  <AuthStack.Navigator>
      <AuthStack.Screen name="Home" component={Home} options={{ title: 'Home' }} />
      <AuthStack.Screen name="Detail" component={Detail} options={{ title: 'Detail' }} />
    </AuthStack.Navigator>
)

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen name="Login" component={Login} />
        <Drawer.Screen name="Home" component={HomeDetailScreen} />
        <Drawer.Screen name="Profile" component={Profile} />
      </Drawer.Navigator>
      
    </NavigationContainer>
  );
}
