import * as React from "react";
import { View, Text, Image, StyleSheet, TextInput, Button } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

export default class Detail extends React.Component {
    state ={
        data:[],
    };

    constructor(props) {
        super(props)
        
    }


    componentDidMount() {
          this.fetchData();
    }
    
    fetchData = async() => {
          const response = await fetch('https://api.jikan.moe/v3/anime/'+this.props.route.params.id+'/');
          const json = await response.json();
          this.setState({
            data: json
          });
    }

  

  render() {
    // const { id }
    // alert(this.props.route.params.id);
    return (
        <View style={styles.container}>
            <ScrollView style={{height:500}}>
            <View style={styles.header}>
                <Text style={{fontSize:25}}>Detail</Text>
            </View>
            <View style={styles.cardcontainer}>
                <View style={styles.profile}>
                    <Text style={{ fontSize:20, textAlign:'center' }}> {this.state.data.title}</Text>
                </View>
                <View style={styles.card}>
                    <Image source={{uri:this.state.data.image_url}} style={{width:120, height: 220, marginRight:10}} />
                    <View style={styles.cardtext}>
                        <Text style={{fontSize:25, width:180}}>Rank : {this.state.data.rank}</Text>
                        <Text style={{marginTop:20, marginBottom:20, color:'red'}}> {this.state.data.episodes} Episode</Text>
                        <View style={{}}>
                            <Text style={{fontSize:30}}>Score </Text>
                            <Text style={{fontSize:50, color:'green'}}>{this.state.data.score}</Text>
                        </View>
                    </View>
                </View>
                
            </View>
            <View style={styles.profile}>
                    <Text style={{ fontSize:20, textAlign:'center' }}> {this.state.data.status} </Text>
            </View>
            <Text style={{fontSize: 30, marginLeft:15}}>Sinopsis</Text>
            
                <Text style={{textAlign:'center', margin:15}}>{this.state.data.synopsis}</Text>
            </ScrollView>
            
        </View>
      );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:20,
        margin:10
    },
    header:{
        padding:10,
        alignItems:'center',
        borderBottomWidth:1
    },
    profile:{
        margin:20,
    },
    card:{
        flexDirection:'row',
        backgroundColor:'grey',
        padding:15,
        borderRadius:10
    },
    cardcontainer:{
        // flexDirection:'row',
        backgroundColor:'grey',
        padding:15,
        borderRadius:10
    }
})
