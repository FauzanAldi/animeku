import * as React from "react";
import { View, Text, Image, StyleSheet, TextInput, Button, FlatList } from "react-native";

import { createStore } from 'redux'

// Membuat action types
const types = {
    INCREMENT: 'INCREMENT',
  }
  
  // Membuat reducer
  const reducer = (state, action) => {
    if (action.type === types.INCREMENT) {
      return { count: state.count + 1 }
    }
    return state
  }
  
  // Mendefinisikan initial state dari store
  const initialState = { count: 0 }

export default class Home extends React.Component {
  state ={
      data:[],
  };


  componentDidMount() {
      this.fetchData();
  }

  fetchData = async() => {
      const response = await fetch('https://api.jikan.moe/v3/genre/anime/1/1');
      const json = await response.json();
      this.setState({
        data: json
      });
  }

    render(){
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={{fontSize:25}}>Selamat Datang di Animeku</Text>
                </View>
                {/* <View style={{marginTop:10, flexDirection:'row-reverse'}}>
                    <Button title="Profile ->"/>
                </View> */}
                <View style={styles.profile}>
                    <Text style={{ fontSize:20, textAlign:'center' }}> List Anime Terpopuler</Text>
                </View>
                <FlatList
                    data={this.state.data.anime}
                    renderItem={(anime)=>
                    <View style={styles.card}>
                        <Image  source={{uri:anime.item.image_url}} style={{width:120, height: 220, marginRight:10}} />
                        <View style={styles.cardtext}>
                            <Text style={{fontSize:25, width:180}}>{ anime.item.title }</Text>
                            <Text style={{marginTop:20, marginBottom:20, color:'red'}}>{ anime.item.episodes } Episode</Text>
                            <Button title='Detail' onPress={() => this.props.navigation.push("Detail", { id: anime.item.mal_id })} />
                        </View>
                    </View>
                    }
                    keyExtractor={(anime)=>anime.mal_id}
                    ItemSeparatorComponent={()=><View style={{height:0.5, backgroundColor:'#cccccc'}} />}
                />
                
                
                
            </View>
          );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:20,
        margin:10
    },
    header:{
        padding:10,
        alignItems:'center',
        borderBottomWidth:1
    },
    profile:{
        margin:20,
    },
    card:{
        flexDirection:'row',
        backgroundColor:'grey',
        padding:15,
        borderRadius:10,
        marginBottom:10
    }
})
