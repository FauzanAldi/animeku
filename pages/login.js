import * as React from "react";
import { View, Text, Image, StyleSheet, TextInput, Button } from "react-native";
import { createStore } from 'redux'
import { ScrollView } from "react-native-gesture-handler";

// Membuat action types
const types = {
    INCREMENT: 'INCREMENT',
  }
  
  // Membuat reducer
  const reducer = (state, action) => {
    if (action.type === types.INCREMENT) {
      return { count: state.count + 1 }
    }
    return state
  }
  
  // Mendefinisikan initial state dari store
  const initialState = { count: 0 }

export default function Login({ navigation }) {
  return (
    <View style={styles.container}>
        <ScrollView>
        <View style={styles.header}>
            <Text style={{fontSize:30, marginTop:20, marginBottom:20}}>Animeku</Text>
            <Image source={require('./../assets/logo.jpg')} style={{height:150, width:300}} />
            <Text style={{fontSize:20, marginTop:10}}>Silahkan Login untuk Masuk</Text>
            <View style={{marginLeft:20, marginRight:20}}>
                <View style={{paddingTop:20}}>
                    <Text style={{marginTop:10, marginBottom:10}}>
                        Username
                    </Text>
                    <TextInput style={styles.inputText} />
                </View>
                <View style={{paddingTop:20}}>
                    <Text style={{marginTop:10, marginBottom:10}}>
                        Password
                    </Text>
                    <TextInput style={styles.inputText} />
                </View>
                <View style={{marginTop:50, width:100, alignSelf:'center'}}>
                    <Button title='Masuk' style={{borderRadius:10}} onPress={() => navigation.navigate('Home')} />
                </View>
            </View>
        </View>
        </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:20
    },
    header:{
        alignItems:'center',
    },
    inputText:{
        borderColor:'black',
        borderWidth:1,
        borderRadius:5,
        height:40,
        padding:10,
        width:200
        
    },
})
