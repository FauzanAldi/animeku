import * as React from "react";
import { View, Text, Image, StyleSheet, TextInput, Button } from "react-native";

import { createStore } from 'redux'

// Membuat action types
const types = {
    INCREMENT: 'INCREMENT',
  }
  
  // Membuat reducer
  const reducer = (state, action) => {
    if (action.type === types.INCREMENT) {
      return { count: state.count + 1 }
    }
    return state
  }
  
  // Mendefinisikan initial state dari store
  const initialState = { count: 0 }

export default function Profile() {
  return (
    <View style={styles.container}>
        <View style={styles.header}>
            <Text style={{fontSize:30}}>Tentang Kami</Text>
        </View>
        <View style={styles.profile}>
            <Text style={{ fontSize:20, textAlign:'center' }}> Animeku adalah  Infomasi tentang anime terbaik sepanjang masa</Text>
        </View>
        <Image source={require('./../assets/owner.jpg')} style={{width:200, height:300, alignSelf:'center'}}></Image>
        <View style={styles.contact}>
        <Text style={styles.contactsize}>Mohamad Fauzan Aldi</Text>
            <Text style={styles.contactsize}>Contact</Text>
            <Text style={styles.contactsize}>085888295270</Text>
            <Text style={styles.contactsize}>(Whatsapp)</Text>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:20,
        margin:10
    },
    header:{
        padding:10,
        alignItems:'center',
        borderBottomWidth:1
    },
    profile:{
        margin:20,
        // alignContent:'center',
        // alignItems:'ju',
        // alignSelf:'center'
    },
    contact:{
        alignItems:'center',
        
    },
    contactsize:{
        fontSize:20
    }
})
